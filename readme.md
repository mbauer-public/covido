# SWENGS Project Covido

A delivery service for covid 19 patients made possible by volunteers

How to get this project correctly into IntellJ: 

1.) Create a "Covido" folder on your local drive
<br>
2.) Execute "git clone https://github.com/holzerma18/covido.git" inside the "Covido" folder to clone the repository.
<br>
3.) Open IntellJ and open the project (File -> Open -> select correct path -> OK)
<br>
4.) Setup a virtualenv for Covido
<ol>
    <li>Open the Project Structure (File -> Project Structure)</li>
    <li>Select "SDKs" on the left side</li>
    <li>Click "+" -> "Add Python SDK"</li>
    <li>Select "New Environment" and make  sure to use this location: \covido\backend\venv
    <li>Klick "OK" the create the virtual environment</li>
    <li>In the project Structure, select "Project" on the left side</li>
    <li>Now select the created VENV as Project SDK"</li>
</ol>
5.) Import frontend module
<ol>
    <li>Open the Project Structure again and select "Modules" on the left side</li>
    <li>Click "+" -> "Import Module"</li>
    <li>Select the frontend folder within the covido project and click "Next" -> "Next and then "Finish"
    <li>Click apply to save changes</li>
</ol>
6.) Import backend module
<ol>
    <li>Open the Project Structure again and select "Modules" on the left side</li>
    <li>Click "+" -> "Import Module"</li>
    <li>Select the backend folder within the covido project and click "Next" -> "Next and then "Finish"
    <li>Click apply to save changes</li>
    <li>In the Django properties, make sure that the following parameters are correct</li>
    <li>- Django project root is "C:\<pathToCovidoOnOurDisk>\backend"</li>
    <li>- Path to settings is "backend\settings.py"</li>
    <li>- Manage script is manage.py "backend\manage.py"</li>
</ol>
6.) Import dependencies
<ol>
    <li>- dependencies are in "frontend\package.json" and "backend\requirements.txt"</li>
</ol>
7.) Run Django Server and Angular CLI Server and have fun :-)
<ol>
    <li>- default admin user if you also got the db.sqlite3 file: admin@covido.local with PW "admin"</li> 
    <li>- To add or remove credit from a user, the API endpoint localhost:8000/paymenttransactions must be called with the admin user.</li>
    <li>- If you want to add credit, select the user as "receiver" and enter the amount. The other fields remain Null.</li>
    <li>- If you want to remove credit, select the user as "sender" and enter the amount. The other fields stay Null.
</ol>
