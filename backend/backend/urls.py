"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from . import settings
from .covido import views
from .covido.views import MediaDownloadView

router = routers.DefaultRouter()
router.register(r'covidousers', views.CovidoUserViewSet)
router.register(r'items', views.ItemViewSet)
router.register(r'shoppinglists', views.ShoppingListViewSet)
router.register(r'ratings', views.RatingViewSet)
router.register(r'media', views.MediaViewSet)
router.register(r'paymenttransactions', views.PaymentTransactionViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
    path('media/download/<int:pk>/', MediaDownloadView.as_view()),
    path(r'register/', views.register),
    path(r'statistics/', views.statistics)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Example usage of API:
# curl -X POST -d "username=admin&password=admin" http://localhost:8000/api-token-auth/
# curl -X GET "http://localhost:8000/movies/" -H "accept: application/json" -H "Authorization: Bearer <TOKEN>
# Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjA5MDU1ODkxLCJlbWFpbCI6IiJ9.RWv8qi9A1sy71wMBps0Qqp1ndaG10YxslKqPSL1MifI
# Superuser erstellen (Windows): \backend\venv\Scripts\python.exe manage.py createsuperuser
