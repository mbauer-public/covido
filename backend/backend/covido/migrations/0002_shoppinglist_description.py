# Generated by Django 3.1.3 on 2020-12-20 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('covido', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoppinglist',
            name='description',
            field=models.CharField(default='Another Covido ShoppingList', max_length=255),
        ),
    ]
