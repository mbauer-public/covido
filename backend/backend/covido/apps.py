from django.apps import AppConfig


class CovidoConfig(AppConfig):
    name = 'backend.covido'
