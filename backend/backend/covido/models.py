from django.contrib.auth import get_user_model
from django.db import models
from datetime import datetime
from django.contrib.auth.models import User, AbstractUser, UserManager


'''
Custom UserManager for our CovidoUser
override create_user and create_superuser to support our required fields
'''
class CovidoUserManager(UserManager):
    def create_user(self, email, city, zipcode, street_name, street_number, password=None):
        """
        Creates and saves a User with all required fields
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email), # make email case insensitive
            city=city,
            zipcode=zipcode,
            street_name=street_name,
            street_number=street_number,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, city, zipcode, street_name, street_number, password=None):
        """
        Creates and saves a superuser with all required fields
        """
        user = self.create_user(
            email,
            city,
            zipcode,
            street_name,
            street_number,
            password=password,
        )
        user.is_superuser = True;
        user.is_staff = True;
        user.save(using=self._db)
        return user

'''
CovidoUser is our own custom user model
'''
class CovidoUser(AbstractUser):

    ROLE_ADMINISTRATOR = 0
    ROLE_AFFECTED = 1
    ROLE_VOLUNTEER = 2

    ROLE_CHOICES = (
        (ROLE_ADMINISTRATOR, 'Administrator'),
        (ROLE_AFFECTED, 'Affected'),
        (ROLE_VOLUNTEER, 'Volunteer')
    )

    email = models.EmailField(max_length=255, unique=True)
    username = models.CharField(default=None, max_length=16, null=True) # override the default username field
    credit = models.DecimalField(max_digits=7, decimal_places=2, default=0.0)
    city = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=4)
    street_name = models.CharField(max_length=255)
    street_number = models.CharField(max_length=5)
    role = models.IntegerField(choices=ROLE_CHOICES, default=ROLE_AFFECTED)
    ratings = models.ManyToManyField('self', through='Rating')

    # We use email as username
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['city', 'zipcode', 'street_name', 'street_number']
    objects = CovidoUserManager()

    def __str__(self):
        return self.email

'''
class SiteUser(models.Model):
    ROLE_ADMINISTRATOR = 0
    ROLE_AFFECTED = 1
    ROLE_VOLUNTEER = 2

    ROLE_CHOICES = (
        (ROLE_ADMINISTRATOR, 'Administrator'),
        (ROLE_AFFECTED, 'Affected'),
        (ROLE_VOLUNTEER, 'Volunteer')
    )
    user = models.OneToOneField(CovidoUser, on_delete=models.CASCADE)
    credit = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    city = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=4)
    street_name = models.CharField(max_length=255)
    street_number = models.CharField(max_length=5)
    is_locked = models.BooleanField(default=False)
    role = models.IntegerField(choices=ROLE_CHOICES, default=ROLE_AFFECTED)
    ratings = models.ManyToManyField('self', through='Rating')

    # __str__ gibt den username (=email) zurück
    def __str__(self):
        return self.user.email
'''


# m:n Table between User and User ('self')
class Rating(models.Model):
    rater = models.ForeignKey(CovidoUser, related_name='rater', on_delete=models.CASCADE)
    rated = models.ForeignKey(CovidoUser, related_name='rated', on_delete=models.CASCADE)
    stars = models.PositiveIntegerField()
    comment = models.TextField(null=True, blank=True)
    is_report = models.BooleanField()


class Media(models.Model):
    file = models.FileField(upload_to='')
    content_type = models.CharField(null=True, blank=True, max_length=100)

    def __str__(self):
        return self.file.name

    def save(self, *args, **kwargs):
        self.content_type = self.file.file.content_type
        super().save(*args, **kwargs)


class ShoppingList(models.Model):
    creator = models.ForeignKey(CovidoUser, null=False, on_delete=models.CASCADE, related_name='creator')
    acceptor = models.ForeignKey(CovidoUser, null=True, on_delete=models.CASCADE, related_name='acceptor')
    description = models.CharField(max_length=255, default='Another Covido ShoppingList')
    comment = models.TextField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    max_budget = models.DecimalField(max_digits=5, decimal_places=2)
    only_bio = models.BooleanField()
    timestamp_published = models.DateTimeField(blank=True, null=True)
    timestamp_accepted = models.DateTimeField(blank=True, null=True)
    timestamp_otwts = models.DateTimeField(blank=True, null=True)
    timestamp_paid = models.DateTimeField(blank=True, null=True)
    timestamp_finished = models.DateTimeField(blank=True, null=True)
    bill = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    pictures = models.ManyToManyField(Media, blank=True)

    def __str__(self):
        return self.description


class Item(models.Model):
    UNIT_CHOICES = (
        ('g', 'Gramm'),
        ('kg', 'Kilogramm'),
        ('ml', 'Milliliter'),
        ('l', 'Liter'),
        ('stk', 'Stück'),
        ('pkg', 'Packung'),
    )

    name = models.CharField(max_length=255)
    amount = models.PositiveIntegerField(default=1)
    unit = models.CharField(max_length=3, choices=UNIT_CHOICES, default='g')
    shopping_list = models.ForeignKey(ShoppingList, related_name='items', on_delete=models.CASCADE) # A specific item (name, amount, unit) belongs to one specific shopping list.

    def __str__(self):
        return self.name;


class PaymentTransaction(models.Model):
    receiver = models.ForeignKey(CovidoUser, related_name="payments_received", null=True, on_delete=models.PROTECT) # This field will be null if credit is withdrawn by an admin.
    sender = models.ForeignKey(CovidoUser, related_name="payments_sent", null=True, on_delete=models.PROTECT) # This field will be null if credit is added by an admin.
    shopping_list = models.ForeignKey(ShoppingList, null=True, on_delete=models.SET_NULL) # This will also be null for admin-actions or if the shopping list was deleted.
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
