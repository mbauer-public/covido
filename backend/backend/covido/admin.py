from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models


# Register your models here.


class CovidoUserAdmin(admin.ModelAdmin):
    list_display = (
    'pk', 'email', 'first_name', 'last_name', 'credit', 'city', 'zipcode', 'street_name', 'street_number',
    'role', 'is_active')


class ShoppingListAdmin(admin.ModelAdmin):
    list_display = (
    'pk', 'creator', 'acceptor', 'description', 'comment', 'is_public', 'max_budget', 'only_bio', 'timestamp_published',
    'timestamp_accepted', 'timestamp_otwts', 'timestamp_paid', 'timestamp_finished', 'bill',
    )


class RatingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'rater', 'rated', 'stars', 'comment', 'is_report')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'amount', 'unit', 'shopping_list')


class PaymentTransactionAdmin(admin.ModelAdmin):
    list_display = ('pk', 'receiver', 'sender', 'amount', 'timestamp')


admin.site.register(models.CovidoUser, CovidoUserAdmin)
admin.site.register(models.ShoppingList, ShoppingListAdmin)
admin.site.register(models.Rating, RatingAdmin)
admin.site.register(models.Item, ItemAdmin)
admin.site.register(models.PaymentTransaction, PaymentTransactionAdmin)
