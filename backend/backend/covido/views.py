import os

from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponse
from django.views import View
from rest_framework import viewsets, status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from . import serializers
from . import models
from .models import CovidoUser, Media, ShoppingList
from .serializers import CovidoUserSerializer
from django.db.models import Q
from django.db import transaction, IntegrityError
"""
 ModelViewSet provides the following actions:
  `create()`, -> POST
  `retrieve()`, -> GET (exactly one via pk)
  `update()`, -> PUT 
  `partial_update()`, -> PUT
  `destroy()` -> DELETE
  `list() -> GET
 """


class CovidoUserViewSet(viewsets.ModelViewSet):
    queryset = models.CovidoUser.objects.all()

    serializer_class = serializers.CovidoUserSerializer

    # Handle single element calls - /covidouser/<pk>
    # Volunteer has to call this to get affecteds address
    def retrieve(self, request, pk):

        # get the user from the request
        user = request.user

        # get data of the requested user
        req_user = self.filter_queryset(self.get_queryset()).filter(pk=pk).first()

        # if the user requested his own data, return it
        if req_user.id == user.id:
            return Response(self.serializer_class(req_user).data)

        qs_shoppinglists = self.filter_queryset(models.ShoppingList.objects.all())

        # all shoppinglists where request.user is acceptor and creator is the requested user
        qs_shoppinglists = qs_shoppinglists.filter(Q(acceptor_id=user.id) & Q(creator_id=req_user.id))

        # check if at least one ShopppingList has status "paid" -> if yes, the volunteer is allowed to access information about the affected.
        qs_shoppinglists = qs_shoppinglists.filter(~Q(timestamp_paid=None))

        if qs_shoppinglists:
            # there is at least one list with status paid
            # return data of requested user
            return Response(self.serializer_class(req_user).data)

        # access forbidden
        return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super(CovidoUserViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class ItemViewSet(viewsets.ModelViewSet):
    queryset = models.Item.objects.all()

    serializer_class = serializers.ItemSerializer

    def destroy(self, request, *args, **kwargs):
        shopping_list_creator_id = models.Item.objects.get(pk=kwargs.get('pk')).shopping_list.creator.id
        if request.user.is_superuser or request.user.id == shopping_list_creator_id:
            return super(ItemViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

class ShoppingListViewSet(viewsets.ModelViewSet):
    queryset = models.ShoppingList.objects.all()

    serializer_class = serializers.ShoppingListSerializer

    def list(self, request):

        # get the user from the request
        user = request.user

        queryset = self.filter_queryset(self.get_queryset())

        show_only_in_same_district = self.request.query_params.get('showOnlyInSameDistrict', None)

        # Exclude all finished shoppinglists
        queryset = queryset.filter(timestamp_finished=None)
        # Roles: 0 = Admin, 1 = Affected, 2 = Volunteer
        # Important - all extended relations are handled using the site_user extension to the django base user
        # If the request user has the admin flag set we'll allow him to see everything
        if user.is_superuser:
            pass
        # If the users site_user role is Affected we'll only show him his own Shoppinglists
        elif user.role == 1:
            queryset = queryset.filter(creator_id=user.id)
        # If the user is a volunteer we'll show him all public shopping lists as well as all
        # lists that feature him as the acceptor
        elif user.role == 2:
            queryset = queryset.filter(Q(is_public=True, acceptor=None) | Q(acceptor_id=user.id))

            if show_only_in_same_district == 'true':
                queryset = queryset.filter(Q(is_public=True, acceptor=None, creator__zipcode=user.zipcode) | Q(acceptor_id=user.id))
            else:
                queryset = queryset.filter(Q(is_public=True, acceptor=None, creator__zipcode__startswith=user.zipcode[0:2]) | Q(acceptor_id=user.id))

        # In any other case we'll return nothing
        else:
            queryset = None
        return Response(self.serializer_class(queryset, many=True).data)

    # Handle single element calls - example /shoppinglistform/<pk> -> retrieves exactly one element
    def retrieve(self, request, pk):
        user = request.user
        queryset = self.filter_queryset(self.get_queryset()).filter(pk=pk).first()

        # Get the id of the creator of the requested shoppinglist
        requested_shoppinglist_creator_id = queryset.creator_id

        # Admins should be able to edit and view any shopping lists
        if user.is_superuser:
            return Response(self.serializer_class(queryset).data)

        # See if the creator_id of the shoppinglist matches the requesting user
        elif user.id == requested_shoppinglist_creator_id:
            return Response(self.serializer_class(queryset).data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def create(self, request):
        user = request.user
        # If a shoppinglist is created, take the pk of the current user for the creator-field
        if user.role == 1 or user.is_superuser:
            request.data.update({"creator": request.user.pk})

            queryset = self.get_queryset()
            user = request.user
            # Calculate how much money a user has set as max_budget for all shoppinglists combined
            overall_max_budget = float(request.data['max_budget']) + \
                                 sum(list(map(lambda di: float(di['max_budget']),
                                              queryset.filter(creator=user.id, timestamp_finished__isnull=True)
                                              .values("max_budget"))))
            if overall_max_budget <= user.credit:
                return super().create(request)
            else:
                return Response(data={"info": "Sum of max Budget for all lists is greater than available credit!"},
                                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(data={"info": "Only User with role affected are allowed to create shopping lists."},
                            status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, *args, **kwargs):
        user = request.user
        instance = self.queryset.get(pk=kwargs.get('pk'))
        queryset = self.get_queryset()
        overall_max_budget = float(request.data['max_budget']) + \
                             sum(list(map(lambda di: float(di['max_budget']),
                                          queryset.filter(creator=user.id, timestamp_finished__isnull=True)
                                          .values("max_budget")))) - float(instance.max_budget)

        if request.data['acceptor'] is None and request.data['creator'] is not user.id:
            request.data.update({'acceptor': user.id})

        # If no timestamp_accepted is set in our database we need to allow updates
        # We also need to allow updates if the current user is the creator or acceptor of the list
        if instance.creator == user or instance.acceptor == user or instance.timestamp_accepted is None:
            # If the timestamp_accepted isn't set but the request sent contains the current user as acceptor
            # and the timestamp_accepted in the request is set we'll allow updates as this means a user has just
            # pressed accept
            if instance.timestamp_accepted is None and request.data['timestamp_accepted'] is not None \
                    or instance.creator == user:
                # Then we need to do a budget check
                if overall_max_budget <= instance.creator.credit:

                    # Create a new Payment Transaction if the last update
                    if request.data["timestamp_finished"] is not None \
                            and not models.PaymentTransaction.objects.all().filter(shopping_list=instance.id).exists():
                        # Retrieve User Data and add/subtract credit
                        receiver_instance = models.CovidoUser.objects.get(pk=instance.acceptor_id)
                        receiver_instance.credit = receiver_instance.credit + instance.bill

                        sender_instance = models.CovidoUser.objects.get(pk=instance.creator_id)
                        sender_instance.credit = sender_instance.credit - instance.bill
                        # Treat the payment process as a transaction - this avoids the problem of
                        # Wrong payments being made due to crashes/execeptions
                        try:
                            with transaction.atomic():
                                models.PaymentTransaction(sender=instance.creator,
                                                          receiver=instance.acceptor,
                                                          amount=instance.bill,
                                                          shopping_list=instance,
                                                          ).save()
                                receiver_instance.save()
                                sender_instance.save()
                        except IntegrityError:
                            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                            data={"info": "Payment could not be processed"})


                    return super().partial_update(request, *args, **kwargs)
                else:
                    return Response(data={"info": "Sum of max Budget for all lists is greater than available credit!"},
                                    status=status.HTTP_403_FORBIDDEN)
            # Verify that the request data that is sent for update by a volunteer isn't manipulated
            elif instance.acceptor == user and instance.creator.id == request.data["creator"] \
                    and instance.acceptor.id == request.data["acceptor"] \
                    and instance.description == request.data["description"] \
                    and instance.comment == request.data["comment"] \
                    and instance.is_public == request.data["is_public"] \
                    and instance.max_budget == float(request.data["max_budget"]):

                return super().partial_update(request, *args, **kwargs)
            else:
                return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, *args, **kwargs):
        shopping_list_creator_id = models.ShoppingList.objects.get(pk=kwargs.get('pk')).creator.id
        if request.user.is_superuser or request.user.id == shopping_list_creator_id:
            return super(ShoppingListViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class RatingViewSet(viewsets.ModelViewSet):
    queryset = models.Rating.objects.all()

    serializer_class = serializers.RatingSerializer

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super(RatingViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)

    def update(self):
        Response(status=status.HTTP_403_FORBIDDEN)


class PaymentTransactionViewSet(viewsets.ModelViewSet):
    queryset = models.PaymentTransaction.objects.all()

    serializer_class = serializers.PaymentTransactionSerializer

    def list(self, request):
        # get the user from the request
        user = request.user

        queryset = self.filter_queryset(self.get_queryset())
        queryset_receiver = queryset.filter(receiver_id=user.id)  # all transactions where user is the payment receiver
        queryset_sender = queryset.filter(sender_id=user.id)  # all transactions where user is payment sender

        # get all payments where the current user is the receiver or sender
        queryset = queryset.filter(Q(sender_id=user.id) | Q(receiver_id=user.id))

        # return filtered queryset
        #from django.db.models import Sum
        #all_sum = queryset_receiver.aggregate(Sum('amount'))['amount__sum'] - queryset_sender.aggregate(Sum('amount'))[
        #    'amount__sum'] if queryset_receiver and queryset_sender else 0

        queryset_shoppinglist = models.ShoppingList.objects.all()
        currently_committed_budget = sum(list(map(lambda di: float(di['max_budget']),
                                                  queryset_shoppinglist.filter(creator=user.id,
                                                                               timestamp_finished__isnull=True)
                                                  .values("max_budget"))))
        free_balance = float(user.credit) - float(currently_committed_budget)

        return Response({'balance': user.credit, 'free_balance': free_balance,
                         'transactions': self.serializer_class(queryset, many=True).data})

    def create(self, request, *args, **kwargs):
        user = request.user
        # Allow admins to manually create transactions
        if user.is_superuser:
            if request.data["sender"] is not None and (request.data["receiver"] is None or request.data["receiver"] == '') \
                    and (request.data["shopping_list"] is None or request.data["shopping_list"] == ''):
                try:
                    with transaction.atomic():
                        user_instance = models.CovidoUser.objects.get(pk=request.data["sender"])
                        user_instance.credit = float(user_instance.credit) - float(request.data["amount"])
                        user_instance.save()
                        return super(PaymentTransactionViewSet, self).create(request, *args, **kwargs)
                except IntegrityError:
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                    data={"info": "Payment could not be processed"})

            elif request.data["receiver"] is not None and (request.data["sender"] is None or request.data["sender"] == '') \
                    and (request.data["shopping_list"] is None or request.data["shopping_list"] == ''):
                try:
                    with transaction.atomic():
                        user_instance = models.CovidoUser.objects.get(pk=request.data["receiver"])
                        user_instance.credit = float(user_instance.credit) + float(request.data["amount"])
                        user_instance.save()
                        return super(PaymentTransactionViewSet, self).create(request, *args, **kwargs)
                except IntegrityError:
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                    data={"info": "Payment could not be processed"})
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super(PaymentTransactionViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)

    def update(self):
        Response(status=status.HTTP_403_FORBIDDEN)


class MediaViewSet(viewsets.ModelViewSet):
    queryset = Media.objects.all()
    serializer_class = serializers.MediaSerializer

    def pre_save(self, obj):
        obj.file = self.request.FILES.get('file')

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super(MediaViewSet, self).destroy(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class MediaDownloadView(View):
    def get(self, request, pk):
        media = Media.objects.get(pk=pk)
        content_type = media.content_type
        response = HttpResponse(media.file.file, content_type=content_type)
        original_file_name = os.path.basename(media.file.name)
        response['Content-Disposition'] = 'inline; filename=' + original_file_name
        return response


'''
register(request) is used to create a new user.
It only reacts to POST request and access is allowed for everyone
'''


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    serializer = CovidoUserSerializer(data=request.data)
    if serializer.is_valid():  # check if data for new user is valid
        new_user = serializer.save()  # create and save new user
        return Response(serializer.data, status=status.HTTP_201_CREATED)  # return user-data
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  # return errors


'''
statistics(request) will be used for requests regarding user-stats
for example: # of fulfilled orders, # bought items
'''


@api_view(['GET'])
def statistics(request):
    user = request.user
    model = models.ShoppingList.objects.all()
    number_of_trips = model.filter(acceptor=user.id, timestamp_finished__isnull=False).count()
    # Gather all item lists from all shopping lists the user was involved in that are finished
    # Count each list of items, then sum up the sums of all item lists
    number_of_items = model.filter(acceptor=user.id, timestamp_finished__isnull=False).values("items").values().count()
    amount_paid = sum(list(map(lambda di: float(di['bill']),
                               model.filter(acceptor=user.id,
                                            timestamp_finished__isnull=False).values("bill"))))

    return Response(data={"number_of_trips": number_of_trips,
                          "number_of_items": number_of_items,
                          "amount_paid": amount_paid}, status=status.HTTP_200_OK)
