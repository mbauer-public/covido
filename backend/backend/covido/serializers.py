from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers
from . import models
from .models import Item, ShoppingList, Media


class CovidoUserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    free_balance = serializers.SerializerMethodField()

    def validate_first_name(self, value):
        if len(value) < 2:
            raise serializers.ValidationError("First name must consist of at least 2 characters.")
        return value

    def validate_last_name(self, value):
        if len(value) < 2:
            raise serializers.ValidationError("Last name must consist of at least 2 characters.")
        return value

    def validate_street_name(self, value):
        if len(value) < 2:
            raise serializers.ValidationError("Street name must consist of at least 2 characters.")
        return value

    def validate_street_number(self, value):
        if not any(char.isdigit() for char in value):
            raise serializers.ValidationError("Street number must consist of at least 1 number.")
        return value

    class Meta:
        model = models.CovidoUser;
        fields = '__all__'

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = models.CovidoUser(**validated_data)
        user.set_password(password)
        user.save()
        return user

    def get_free_balance(self, obj):
        user = obj
        queryset = models.ShoppingList.objects.all()
        currently_committed_budget = sum(list(map(lambda di: float(di['max_budget']),
                                                  queryset.filter(creator=user.id, timestamp_finished__isnull=True)
                                                  .values("max_budget"))))
        return float(user.credit) - float(currently_committed_budget)

'''
class SiteUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SiteUser
        fields = ['pk', 'credit', 'zipcode',
                  'street_name', 'street_number', 'is_locked', 'role', 'ratings']
'''


class RatingSerializer(serializers.ModelSerializer):

    def validate_stars(self, value):
        if value > 5 or value < 1:
            raise serializers.ValidationError("Stars must be between 1 and 5.")
        return value

    class Meta:
        model = models.Rating
        fields = ['pk', 'rater', 'rated', 'stars', 'comment', 'is_report']


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Item
        fields = ['pk', 'name', 'amount', 'unit', 'shopping_list']


class ShoppingListSerializer(serializers.ModelSerializer):

    items = ItemSerializer(many=True, read_only=True)
    # Add a SerializerMethodField that will be evaluated at runtime
    # This allows us to add dynamic data to our Serializers -
    affected_address = serializers.SerializerMethodField()
    picture_urls = serializers.SerializerMethodField()

    def validate_max_budget(self, value):
        if value < 0.00:
            raise serializers.ValidationError("Max budget must be zero or positive.")
        return value

    def validate_bill(self, value):
        if value and value < 0.00:
            raise serializers.ValidationError("Bill must be zero or positive.")
        return value

    class Meta:
        model = models.ShoppingList
        fields = ['pk', 'creator', 'acceptor', 'description', 'comment', 'is_public', 'max_budget', 'only_bio', 'timestamp_published',
                  'timestamp_accepted', 'timestamp_otwts', 'timestamp_paid', 'timestamp_finished', 'bill',
                  'items', 'pictures', 'affected_address', 'picture_urls']

    # Get_affected_address is called once data is requested via this serializer
    def get_affected_address(self, obj):
        # The method evaluates if the timestamp_paid field is filled, if so this means
        # The acceptor of a shoppinglist is allowed to see the affecteds address
        # If not we will simply return Null/None
        if obj.timestamp_paid is not None and obj.timestamp_finished is None:
            return {"first_name": obj.creator.first_name,
                    "last_name": obj.creator.last_name,
                    "zipcode": obj.creator.zipcode,
                    "city": obj.creator.city,
                    "street_name": obj.creator.street_name,
                    "street_number": obj.creator.street_number}
        else:
            return None

    def get_picture_urls(self, obj):
        if obj.pictures is not None:
            return list(map(lambda picture: picture['file'], obj.pictures.values()))
        else:
            return None



class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = ['id', 'file', 'content_type']


class PaymentTransactionSerializer(serializers.ModelSerializer):

    receiver_name = serializers.ReadOnlyField(source='receiver.first_name')
    sender_name = serializers.ReadOnlyField(source='sender.first_name')
    reference = serializers.ReadOnlyField(source='shopping_list.description')

    class Meta:
        model = models.PaymentTransaction
        fields = '__all__'

