import { Component, OnInit } from '@angular/core';
import {User} from "../services/user.service";
import {HttpClient} from "@angular/common/http";

interface Stats {
  number_of_trips: number;
  number_of_items: number;
  amount_paid: number;
}


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  number_of_trips: number;
  number_of_items: number;
  amount_paid: number;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<any>('/api/statistics/').subscribe((stats: Stats) =>
    {
      this.number_of_trips = stats.number_of_trips;
      this.number_of_items = stats.number_of_items;
      this.amount_paid = stats.amount_paid;
      alert('Amount Paid: ' + this.amount_paid + '\n' +
            'Number of Items: ' + this.number_of_items + '\n' +
            'Number of Trips: ' + this.number_of_trips);
    });
  }

}
