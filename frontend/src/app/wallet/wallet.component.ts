import { Component, OnInit } from '@angular/core';
import {PaymentTransaction, Wallet, WalletService} from '../services/wallet.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  wallet: Wallet;
  displayedColumns = ['icon', 'date', 'receiver_name', 'sender_name', 'amount', 'reference'];
  constructor(public walletService: WalletService, public userService: UserService) { }

  ngOnInit(): void {
    this.walletService.getWallet()
      .subscribe( (wallet) => {
        // Sort PaymentTransactions by id (and therefore by date)
        wallet.transactions = wallet.transactions.sort((a, b) => (a.id < b.id) ? 1 : -1);
        this.wallet = wallet;
      });
  }

}
