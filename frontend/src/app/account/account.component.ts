import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {User, UserService} from '../services/user.service';

interface Stats {
  number_of_trips: number;
  number_of_items: number;
  amount_paid: number;
}

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  accountFormGroup;
  user;
  statistics: Stats;

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router,
              public userService: UserService) {
  }

  ngOnInit(): void {

    if (this.userService.isLoggedIn.getValue() === false) {
      this.router.navigate(['login']);
    }
    this.accountFormGroup = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      city: new FormControl('', [Validators.required, Validators.pattern(/^([^0-9]*)$/)]),
      zipcode: new FormControl('', Validators.pattern(/^\d{4}$/)),
      street_name: new FormControl('', Validators.required),
      street_number: new FormControl('', Validators.required),
    });

    this.userService.getCurrentUser().subscribe((user) => {
      this.accountFormGroup.patchValue(user);
    });

    this.http.get<Stats>('/api/statistics/').subscribe((stats: Stats) => {
      this.statistics = stats;
    });
  }
  updateAccount(): void {
    this.userService.updateUser(this.accountFormGroup.value)
      .subscribe(() => {
        this.ngOnInit();
      });
  }
}
