import {Component, OnInit} from '@angular/core';
import {ShoppingList, ShoppingListService, ShoppingStatus} from '../services/shoppinglist.service';
import {UserService} from '../services/user.service';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PaymentBillDialogComponent} from '../payment-bill-dialog/payment-bill-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
import {RatingDialogComponent} from '../rating-dialog/rating-dialog.component';
import {Rating, RatingService} from '../services/rating.service';


@Component({
  selector: 'app-shoppinglist-list',
  templateUrl: './shoppinglist-list.component.html',
  styleUrls: ['./shoppinglist-list.component.scss']
})
export class ShoppinglistListComponent implements OnInit {
  shoppingLists: ShoppingList[];
  showOnlyInSameDistrict;
  displayedColumns = ['creator', 'acceptor', 'comment', 'is_public'];


  hideElement = true;

  constructor(public shoppingListService: ShoppingListService, public userService: UserService,
              public dialog: MatDialog, private snackBar: MatSnackBar, private ratingService: RatingService) {
    this.showOnlyInSameDistrict = true;
  }

  ngOnInit(): void {
    this.shoppingListService.getShoppingLists(this.showOnlyInSameDistrict)
      .subscribe((shoppingLists) => {
        this.shoppingLists = shoppingLists;
      });
  }

  private retrieveShoppingLists(): void {
    this.shoppingListService.getShoppingLists(this.showOnlyInSameDistrict)
      .subscribe((shoppingLists) => {
        this.shoppingLists = shoppingLists;
      });
  }

  deleteShoppinglist(shoppingList: ShoppingList): void {
    this.shoppingListService.deleteShoppingList(shoppingList)
      .subscribe(() => {
        this.retrieveShoppingLists();
        this.ngOnInit();
      });
  }

  /**
   * Handles the MatSlideToggleChange-Event for the is_public toggle
   * ShoppingList-Service will do a Patch request to update the status of the is_public field
   * Technically, this method can be used for all updates regarding a ShoppingList
   */
  onVisibilityChange(event: MatSlideToggleChange, list: ShoppingList): void {
    if (list.items.length > 0) {
      list.is_public = event.checked;
      if (event.checked) {
        list.timestamp_published = moment(new Date()).format('YYYY-MM-DDTHH:mm:ssZ');

      } else {
        list.timestamp_published = null;
      }
      this.shoppingListService.updateShoppingList(list)
        .subscribe(() => {
          if (event.checked) {
            this.showSnackbar('Your ShoppingList \'' + list.description + '\' is now public and waiting for a volunteer! :-)');
          }
        }, (error) => {
          this.showSnackbar('Your ShoppingList \'' + list.description + '\' could not be published.' +
            ' Maybe you do not have enough credit. ' + error,
            true);
          this.retrieveShoppingLists();
        });
    } else {
      event.source.checked = list.is_public;
      this.showSnackbar('Please click \'Edit\' and add at least one item before you publish your list.', true);
    }
  }

  onStatusButtonClick($event: MouseEvent, list: ShoppingList): void {
    // Get current status of ShoppingList
    const currStatus = this.shoppingListService.getStatus(list);

    // If next next status is  'Paid', open bill confirmation dialog
    if (currStatus === ShoppingStatus.OTWTS) {
      const dialogRef = this.dialog.open(PaymentBillDialogComponent);

      const billAmountSub = dialogRef.componentInstance.pbdValueEmitter.subscribe((result) => {

        // Update bill amount using shoppingListService
        list.bill = result.bill_amount;
        list.pictures = result.pictures;
        this.shoppingListService.updateShoppingList(list).subscribe(() => {
          // Just set new status if the confirmation was correct and the bill amount got updated
          this.shoppingListService.setNextStatus(list).subscribe(() => {
            this.retrieveShoppingLists(); // Load lists again, because we wan't to retrieve address data now.
            this.showSnackbar('You are now able to see address details. ' +
              'You receive the money as soon as the affected confirmed the bill too.');
          }, (error) => { // setNextStatus failed
            this.showSnackbar('An error occurred: ' + error, true);
          });
        }, (error) => { // UpdateShoppingList failed
          this.showSnackbar('An error occurred. Bill not confirmed! ' + error, true);
        });
      });

      // If dialog gets closed, unsubscribe from Emitter
      dialogRef.afterClosed().subscribe((result) => {
        billAmountSub.unsubscribe();

        // If the dialog got closed without confirmation -> inform the user
        if (!result) {
          this.showSnackbar('Bill Confirmation cancelled. ShoppingStatus remains \'On the way to shop\'.', true);
        }
      });
      // If next next status is 'Finished', open rating dialog
    } else if (currStatus === ShoppingStatus.PAID) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {shoppingList: list};
      const dialogRef = this.dialog.open(RatingDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((value) => {
        if (value === undefined || value === false) {
          // undefined -> closed trough click outside of the dialog-box, false -> closed through "Cancel" button
          // Affected didn't confirm the bill
          this.showSnackbar('Bill Confirmation and Rating cancelled. ShoppingStatus remains \'Paid\'.', true);
        } else if (value.report === true) {
          // Report
          const rating: Rating = JSON.parse('{ "pk": null, "rater": ' + list.creator + ', ' +
            '"rated": ' + list.acceptor + ', "stars": ' + value.rating + ', "comment": null, "is_report": true}');
          this.ratingService.createRating(rating).subscribe(result => {
            this.showSnackbar('User reported and invoice not confirmed.', true);
          });
        } else {
          // User has confirmed the invoice and submitted a rating.
          this.shoppingListService.setNextStatus(list).subscribe(() => {
            const rating: Rating = JSON.parse('{ "pk": null, "rater": ' + list.creator + ', ' +
              '"rated": ' + list.acceptor + ', "stars": ' + value.rating + ', "comment": null, "is_report": false}');
            this.ratingService.createRating(rating).subscribe(result => {}); // Create Rating in database
            this.showSnackbar('Completed. Thank you for using COVIDO!');
          }, (error) => {
            this.showSnackbar('Payment could not be confirmed! ' + error, true);
            this.retrieveShoppingLists();
          });
        }
      }, (error) => {
        this.showSnackbar('An error occurred! ' + error, true);
      });
    } else {
      // Go to next status
      this.shoppingListService.setNextStatus(list).subscribe(() => {
      }, (error) => {
        this.showSnackbar('Next status could not be set! ' + error, true);
        this.retrieveShoppingLists();
      });
    }
  }

  // Called if the Slide-Toggle "showOnlyInSameDistrict" changes"
  onSameDistrictToggleChange(event: MatSlideToggleChange): void {
    this.showOnlyInSameDistrict = event.checked;
    this.retrieveShoppingLists();
  }

  // Show a snackbar info
  showSnackbar(text: string, isWarning: boolean = false, duration: number = 10000): void {
    this.snackBar.open(text, null, {
      duration,
      panelClass: ['mat-toolbar', isWarning ? 'mat-warn' : 'mat-primary']
    });
  }
}
