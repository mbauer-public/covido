import {Component, OnInit} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ShoppingList, Item, ShoppingListService} from '../services/shoppinglist.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User, UserService} from '../services/user.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-shoppinglist-form',
  templateUrl: './shoppinglist-form.component.html',
  styleUrls: ['./shoppinglist-form.component.scss']
})

export class ShoppinglistFormComponent implements OnInit {
  hide = false;
  shoppinglistFormGroup: FormGroup;
  itemFormGroup: FormGroup;

  constructor(public shoppingListService: ShoppingListService, private route: ActivatedRoute,
              private router: Router, private userService: UserService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.shoppinglistFormGroup = new FormGroup({
      pk: new FormControl(null),
      description: new FormControl('Another Covido ShoppingList', Validators.required),
      creator: new FormControl(null),
      acceptor: new FormControl(null),
      comment: new FormControl(''),
      is_public: new FormControl(false),
      max_budget: new FormControl(10, Validators.min(0)),
      only_bio: new FormControl(false),
      timestamp_published: new FormControl(null),
      timestamp_accepted: new FormControl(null),
      timestamp_otwts: new FormControl(null),
      timestamp_paid: new FormControl(null),
      timestamp_finished: new FormControl(null),
      bill: new FormControl(10),
      bill_pictures: new FormControl([]),
      items: new FormControl([]),
    });

    this.itemFormGroup = new FormGroup({
      pk: new FormControl(null),
      name: new FormControl('', Validators.required),
      amount: new FormControl(1, Validators.min(1)),
      unit: new FormControl('g'),
      shopping_list: new FormControl(null),
    });

    const routeDate = this.route.snapshot.data;

    const pkFromUrl = this.route.snapshot.paramMap.get('pk');
    if (pkFromUrl) {
      this.shoppingListService.getShoppingList(parseInt(pkFromUrl, 10))
        .subscribe((shoppinglist) => {
          this.shoppinglistFormGroup.patchValue(shoppinglist);
          this.shoppinglistFormGroup.controls.max_budget.asyncValidator =
            this.enoughCreditValidator(Number.parseFloat(shoppinglist.max_budget.toString())); // Doesn't work with just max_budget :O
        });
    }
    else {
      this.shoppinglistFormGroup.controls.max_budget.asyncValidator = this.enoughCreditValidator();
    }
  }
  toggle(): void {
    this.hide = true;
  }
  createOrUpdateShoppingList(): void {
    const pkFromFormGroup = this.shoppinglistFormGroup.value.pk;
    if (pkFromFormGroup) {
      this.shoppingListService.updateShoppingList(this.shoppinglistFormGroup.value)
        .subscribe(() => {
          this.showSnackbar('ShoppingList data updated!');
          this.shoppinglistFormGroup.controls.max_budget.asyncValidator =
            this.enoughCreditValidator(Number.parseFloat(this.shoppinglistFormGroup.controls.max_budget.value)); // Same here
        });
    } else {
      this.shoppingListService.createShoppingList(this.shoppinglistFormGroup.value)
        .subscribe((shoppingList) => {
          this.router.navigate(['shoppinglist-form/' + shoppingList.pk]);
          this.showSnackbar('ShoppingList created! You can now start to add items!');
        });
    }
  }
  deleteItem(item: Item): void {
    this.shoppingListService.deleteItem(item)
      .subscribe(() => {
        this.ngOnInit();
      });
  }

  createOrUpdateItems(): void {
    const pkFromShoppingList = this.shoppinglistFormGroup.value.pk;
    this.itemFormGroup.value.shopping_list = pkFromShoppingList;
    const pkFromFormGroup = this.itemFormGroup.value.pk;
    if (pkFromFormGroup) {
      this.shoppingListService.updateItem(this.itemFormGroup.value)
        .subscribe(() => {
           this.ngOnInit();
        });
    } else {
      this.shoppingListService.createItem(this.itemFormGroup.value)
        .subscribe((item) => {
          this.ngOnInit();
        });
    }
  }

  // Background check if the user has enough credit
  private enoughCreditValidator(initialMaxBudget: number = 0): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      const currentMaxBudget = control.value;
      return this.userService
        .getCurrentUser()
        .pipe(
          map(user => (user.free_balance + initialMaxBudget - currentMaxBudget < 0) ? {notEnoughCredit: true} : null
          )
        );
    };
  }
  // Show a snackbar info
  showSnackbar(text: string, isWarning: boolean = false, duration: number = 10000): void {
    this.snackBar.open(text, null, {
      duration,
      panelClass: ['mat-toolbar', isWarning ? 'mat-warn' : 'mat-primary']
    });
  }
}

