import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {FormGroup} from '@angular/forms';
import {ShoppingList} from './shoppinglist.service';
import {MatSnackBar} from '@angular/material/snack-bar';

export interface User {
  pk: number;
  username: string;
  first_name: string;
  last_name: string;
  password: string;
  email: string;
  credit: number; // Credit
  free_balance: number; // Available Credit (credit - SUM of max_budget of existing lists)
  city: string;
  zipcode: string;
  street_name: string;
  street_number: string;
  is_locked: boolean;
  role: Role;
}

export enum Role {
  ADMINISTRATOR,
  AFFECTED,
  VOLUNTEER,
  ANONYMOUS
}


@Injectable({
  providedIn: 'root'
})

export class UserService {

  readonly accessTokenLocalStorageKey = 'access_token';
  isLoggedIn = new BehaviorSubject(false);

  constructor(private http: HttpClient, private router: Router, private jwtHelperService: JwtHelperService,
              private snackBar: MatSnackBar ) {
    const token = localStorage.getItem(this.accessTokenLocalStorageKey);
    if (token) {
      console.log('Token expiration date: ' + this.jwtHelperService.getTokenExpirationDate(token));
      const tokenValid = !this.jwtHelperService.isTokenExpired(token);
      this.isLoggedIn.next(tokenValid);
    }
  }

  login(userData: { username: string, password: string }): void {
    this.http.post('/api/api-token-auth/', userData)
      .subscribe((res: any) => {
        this.isLoggedIn.next(true);
        localStorage.setItem(this.accessTokenLocalStorageKey, res.token);
        this.router.navigate(['shoppinglist-list']);
      }, () => {
        this.snackBar.open('Wrong email or password. Please try again!', null, {
          duration: 10000,
          panelClass: ['mat-toolbar', 'mat-warn']
        });
      });
  }

  logout(): void {
    localStorage.removeItem(this.accessTokenLocalStorageKey);
    this.isLoggedIn.next(false);
    this.router.navigate(['/login']);
  }

  register(registerFormGroup: FormGroup): void {
    localStorage.removeItem(this.accessTokenLocalStorageKey);

    this.http.post('/api/register/', registerFormGroup.value)
      .subscribe((res: any) => {
        this.snackBar.open('Successfully registered! You can now login using your credentials.', null, {
          duration: 10000,
          panelClass: ['mat-toolbar', 'mat-primary']
        });
        this.router.navigate(['/login']);
      }, (error) => {
        let msg = 'Registration failed! ';

        if (error !== undefined && error.error !== undefined) {
          if (error.error.email !== undefined) {
            msg += '\n' + error.error.username;
          }
          if (error.error.email !== undefined) {
            msg += '\n' + error.error.email;
          }
          if (error.error.password !== undefined) {
            msg += '\n' + error.error.password;
          }
        } else {
          msg += 'Unknown Error!';
        }
        this.snackBar.open('Registration failed! ' + msg, null, {
          duration: 10000,
          panelClass: ['mat-toolbar', 'mat-warn']
        });
      });
  }
  getCurrentUser(): Observable<User> {
    return this.getUser(this.getUserID());
  }
  getUser(pk: number): Observable<User>
  {
    return this.http.get<User>('/api/covidousers/' + pk + '/');
  }
  updateUser(user: User): Observable<any> {
    return this.http.patch('/api/covidousers/' + this.getUserID() + '/', user);
  }
  getUserID(): number {
    const token = localStorage.getItem(this.accessTokenLocalStorageKey);
    // Only decode if there is a token
    if (token) {
      const payload = this.jwtHelperService.decodeToken(token);
      return payload.user_id;
    }
    return null;
  }
  getUsername(): string {
    const token = localStorage.getItem(this.accessTokenLocalStorageKey);

    // Only decode if there is a token
    if (token) {
      const payload = this.jwtHelperService.decodeToken(token);
      return payload.email;
    }
    return '';
  }
  getRole(): Role {
    const token = localStorage.getItem(this.accessTokenLocalStorageKey);

    // Only decode if there is a token
    if (token) {
      const payload = this.jwtHelperService.decodeToken(token);

      if (payload.role === 0) {
        return Role.ADMINISTRATOR;
      } else if (payload.role === 1) {
        return Role.AFFECTED;
      } else {
        return Role.VOLUNTEER;
      }
    }
    // Return Anonymous if there is no token (e.g. user is not logged in)
    else {
      return Role.ANONYMOUS;
    }
  }

  hasPermission(permission): boolean {
    const token = localStorage.getItem(this.accessTokenLocalStorageKey);
    // Nur wenn ein Token da ist (aka User eingeloggt ist) sollen Permissions überprüft werden
    if (token){
      const decodedToken = this.jwtHelperService.decodeToken(token);
      const permissions =  decodedToken.permissions;
      return permission in permissions;
    }
    return false;
  }

}
