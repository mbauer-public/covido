import { TestBed } from '@angular/core/testing';

import { ShoppingListService } from './shoppinglist.service';

describe('ShoppinglistService', () => {
  let service: ShoppingListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShoppingListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
