import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ShoppingList} from './shoppinglist.service';
import {HttpClient} from '@angular/common/http';

export interface Rating {
  pk: number;
  rater: number;
  rated: number;
  stars: number;
  comment: string;
  is_report: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(private http: HttpClient) { }

  createRating(rating: Rating): Observable<any> {
    return this.http.post<Rating>('/api/ratings/', rating);
  }
}
