import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ShoppingList} from './shoppinglist.service';

export interface Wallet {
  balance: number;
  free_balance: number;
  transactions: PaymentTransaction[];
}
export interface PaymentTransaction {
  id: string;
  amount: number;
  timestamp: string;
  receiver_name: string;
  sender_name: string;
  receiver: number;
  sender: number;
  shopping_list: string;
  reference: string;
}

@Injectable({
  providedIn: 'root'
})

export class WalletService {

  constructor(private http: HttpClient) { }

  getWallet(): Observable<Wallet> {
    return this.http.get<Wallet>('/api/paymenttransactions/');
  }
}
