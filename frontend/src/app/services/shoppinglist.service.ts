import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './user.service';
import * as moment from 'moment';

export interface ShoppingList {
  pk: number;
  creator: number;
  acceptor: number;
  description: string;
  comment: string;
  is_public: boolean;
  max_budget: number;
  only_bio: boolean;
  timestamp_published: string;
  timestamp_accepted: string;
  timestamp_otwts: string;
  timestamp_paid: string;
  timestamp_finished: string;
  bill: number;
  picture_urls: string[];
  pictures: number[];
  items: Item[];
  affected_address: User;
}

export interface Item {
  pk: number;
  name: string;
  amount: number;
  unit: 'g' | 'kg' | 'ml' | 'l' | 'stk' | 'pkg';
  shopping_list: number;
}

export enum ShoppingStatus {
  CREATED = 'CREATED',
  PUBLISHED = 'PUBLISHED',
  ACCEPTED = 'ACCEPTED',
  OTWTS = 'On the way to shop',
  PAID = 'PAID',
  FINISHED = 'FINISHED'
}

@Injectable({
  providedIn: 'root'
})

export class ShoppingListService {
  UnitNames = {g: 'g', kg: 'kg', ml: 'ml', l: 'l', stk: 'stk', pkg: 'pkg'};
  constructor(private http: HttpClient) { }

  getShoppingLists(soisd: boolean = true): Observable<ShoppingList[]> {
    return this.http.get<ShoppingList[]>('/api/shoppinglists/?showOnlyInSameDistrict=' + soisd);
  }

  deleteShoppingList(shoppingList: ShoppingList): Observable<any> {
    return this.http.delete('/api/shoppinglists/' + shoppingList.pk + '/');
  }

  getShoppingList(pk: number): Observable<ShoppingList> {
    return this.http.get<ShoppingList>('/api/shoppinglists/' + pk + '/');
  }

  updateShoppingList(shoppingList: ShoppingList): Observable<any> {
    return this.http.patch('/api/shoppinglists/' + shoppingList.pk + '/', shoppingList);
  }

  createShoppingList(shoppingList: ShoppingList): Observable<any> {
    return this.http.post<ShoppingList>('/api/shoppinglists/', shoppingList);
  }

  createItem(item: Item): Observable<any> {
    return this.http.post<Item>('/api/items/', item);
  }

  updateItem(item: Item): Observable<any> {
    return this.http.patch('/api/items/' + item.pk + '/', item);
  }

  deleteItem(item: Item): Observable<any> {
    return this.http.delete('/api/items/' + item.pk + '/');
  }
  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>('/api/items/');
  }
  getStatus(shoppingList: ShoppingList): ShoppingStatus {
    if (shoppingList.timestamp_finished !== null) {
      return ShoppingStatus.FINISHED;
    }
    else if (shoppingList.timestamp_paid !== null) {
      return ShoppingStatus.PAID;
    }
    else if (shoppingList.timestamp_otwts !== null) {
      return ShoppingStatus.OTWTS;
    }
    else if (shoppingList.timestamp_accepted !== null) {
      return ShoppingStatus.ACCEPTED;
    }
    else if (shoppingList.timestamp_published !== null) {
      return ShoppingStatus.PUBLISHED;
    }
    return ShoppingStatus.CREATED;
  }
  getNextStatus(shoppingList: ShoppingList): string {
    const current = this.getStatus(shoppingList);
    switch (current) {
      case ShoppingStatus.PUBLISHED: return 'Accept';
      case ShoppingStatus.ACCEPTED: return 'On the way to shop';
      case ShoppingStatus.OTWTS: return 'Paid';
      case ShoppingStatus.PAID: return 'Complete';
      default: return '';
    }
  }
  setNextStatus(shoppingList: ShoppingList): Observable<any> {
    const current = this.getStatus(shoppingList);
    const now = moment(new Date()).format('YYYY-MM-DDTHH:mm:ssZ');
    let newStatus: ShoppingStatus;
    switch (current) {
      case ShoppingStatus.PUBLISHED:
        shoppingList.timestamp_accepted = now;
        newStatus =  ShoppingStatus.ACCEPTED;
        break;
      case ShoppingStatus.ACCEPTED:
        shoppingList.timestamp_otwts = now;
        newStatus = ShoppingStatus.OTWTS;
        break;
      case ShoppingStatus.OTWTS:
        shoppingList.timestamp_paid = now;
        newStatus = ShoppingStatus.PAID;
        break;
      case ShoppingStatus.PAID:
        shoppingList.timestamp_finished = now;
        newStatus = ShoppingStatus.FINISHED;
        break;
    }
    return this.updateShoppingList(shoppingList);
  }
}
