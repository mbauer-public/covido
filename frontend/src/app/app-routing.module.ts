import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {TestComponent} from './test/test.component';
import {AuthGuard} from './guards/auth.guard';
import {RegisterComponent} from './register/register.component';
import {ShoppinglistListComponent} from './shoppinglist-list/shoppinglist-list.component';
import {ShoppinglistFormComponent} from './shoppinglist-form/shoppinglist-form.component';
import {WalletComponent} from './wallet/wallet.component';
import {RatingDialogComponent} from './rating-dialog/rating-dialog.component';
import {AccountComponent} from './account/account.component';

const routes: Routes = [
  {path : 'test', component : TestComponent, canActivate: [AuthGuard]},
  {path : 'shoppinglist-list', component : ShoppinglistListComponent, canActivate: [AuthGuard]},
  {path : 'login', component : LoginComponent},
  {path : 'logout', component : LogoutComponent},
  {path : 'register', component: RegisterComponent},
  {path: 'shoppinglist-form', component: ShoppinglistFormComponent, canActivate: [AuthGuard]},
  {path: 'shoppinglist-form/:pk', component: ShoppinglistFormComponent, canActivate: [AuthGuard]},
  {path: 'wallet', component: WalletComponent, canActivate: [AuthGuard]},
  {path: 'rating', component: RatingDialogComponent, canActivate: [AuthGuard]},
  {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
  {path: '', component: ShoppinglistListComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
