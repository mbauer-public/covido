import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Role, UserService} from '../services/user.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],

})


export class RegisterComponent implements OnInit {

  registerFormGroup;

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router,
              private userService: UserService) {
  }

  ngOnInit(): void {
    if (this.userService.isLoggedIn.getValue() === true) {
      this.router.navigate(['']);
    }

    this.registerFormGroup = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      city: ['', [Validators.required, Validators.pattern(/^([^0-9]*)$/)]], // Don't allow numbers here
      zipcode: ['', Validators.pattern(/^\d{4}$/)], // Austrian ZIP code
      street_name: ['', Validators.required],
      street_number: ['', Validators.required],
      email: ['', Validators.email],
      emailConf: ['', Validators.email],
      password: ['', Validators.required],
      passwordConf: ['', Validators.required],
      role: ['', Validators.required],
      tos: [false, Validators.requiredTrue]
    }, {validator: [this.passwordValidator(), this.emailValidator()]});
  }

  register(): void {
    this.userService.register(this.registerFormGroup);
  }

  // Doesn't work properly
  passwordValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors | null => {
      const pw1 = group.get('password').value;
      const pw2 = group.get('passwordConf').value;
      return pw1 === pw2 ? null : {invalidConfirmationPW: true};
    };
  }
  emailValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors | null => {
      const m1 = group.get('email').value;
      const m2 = group.get('emailConf').value;
      return m1 === m2 ? null : {invalidConfirmationEmail: true};
    };
  }
}
