import {Component, ElementRef, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../services/user.service';
import {forkJoin} from 'rxjs';

export interface IMedia {
  id?: number;
  file?: string;
  content_type?: string;
}

@Component({
  selector: 'app-mediainput',
  templateUrl: './mediainput.component.html',
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MediainputComponent),
      multi: true
    }
  ]
})
export class MediainputComponent implements OnInit, ControlValueAccessor {
  @Input()
  accept = '';
  resourceUrl = '/api/media/';
  initializing = true;
  medias: IMedia[];
  uploader: FileUploader;
  onChange = (medias: number[]) => {
    // empty default
  }

  constructor(private userService: UserService, private http: HttpClient, elm: ElementRef) {
  }

  ngOnInit(): void {
    this.uploader = new FileUploader({
      url: this.resourceUrl,
      authToken: 'Bearer ' + localStorage.getItem(this.userService.accessTokenLocalStorageKey),
      autoUpload: true,
    });
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      if (!this.medias) {
        this.medias = [];
      }
      this.medias.push({
        id: null,
        file: item.file.name,
      });
    };
    this.uploader.onSuccessItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) => {
      const uploadedMedia = JSON.parse(response) as IMedia;
      const filename = uploadedMedia.file.split('/')[4]; // Noob variant to get the file name out of the URL ;-)
      const uploadingMedia = this.medias.find(media => !media.id && filename.startsWith(media.file.split('.')[0].replace(/ /g, '_')));
      uploadingMedia.id = uploadedMedia.id;
    };
    this.uploader.onErrorItem  = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) => {
      alert(JSON.parse(response));
    };
    this.uploader.onCompleteAll = () => {
      this.onChange(this.medias.map((m) => {
        return m.id;
      }));
    };
  }

  deleteMedia(index: number): void {
    this.medias.splice(index, 1);
    this.onChange(this.medias.map((m) => {
      return m.id;
    }));
  }

  downloadMedia(media: IMedia): void {
    this.http.get(`${this.resourceUrl}download/${media.id}/`, {responseType: 'blob'}).subscribe((blob: Blob) => {
      const fileURL = URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = fileURL;
      a.download = this.getFilename(media);
      document.body.appendChild(a);
      a.click();
      setTimeout(() => {
        document.body.removeChild(a);
      }, 100);
    });
  }

  getFilename(media: IMedia): string {
    return media.file.split('/').pop();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    // not implemented
  }

  setDisabledState(isDisabled: boolean): void {
    // not implemented
  }

  writeValue(mediaIds: any): void {
    if (!mediaIds || !mediaIds.length) {
      this.initializing = false;
    }
    forkJoin(mediaIds.map((id) => {
      return this.http.get(`${this.resourceUrl}/${id}/`);
    })).subscribe((medias) => {
      this.medias = medias;
      this.initializing = false;
    });
  }
}
