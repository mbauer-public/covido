import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ShoppingList} from '../services/shoppinglist.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-rating-dialog',
  templateUrl: './rating-dialog.component.html',
  styleUrls: ['./rating-dialog.component.scss']
})
export class RatingDialogComponent implements OnInit {

  rating = 3;
  rated = false;
  rdFormGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<RatingDialogComponent>, // reference to the dialog instance
    // Get the injected data
    @Inject(MAT_DIALOG_DATA) public data: any // Inject data passed from parent -> access passed shoppingList via this.data.shoppingList
  ) { }

  ngOnInit(): void {
    this.rdFormGroup = new FormGroup({
      accepted_bill: new FormControl(false, Validators.requiredTrue), // checkbox must be ticked
      // rating: new FormControl(3.0, [Validators.min(0.0), Validators.max(5.0)]) // valid rating necessary
    });
  }
  close(report: boolean = false): void {
    this.dialogRef.close({rating: this.rating, report});
  }

  // little helper func
  onRate(): void {
    this.rated = true;
  }
}
