import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-payment-bill-dialog',
  templateUrl: './payment-bill-dialog.component.html',
  styleUrls: ['./payment-bill-dialog.component.scss']
})
export class PaymentBillDialogComponent implements OnInit {

  pbDFormGroup: FormGroup;
  @Output() pbdValueEmitter: EventEmitter<{ pictures: number[], bill_amount: number }>
    = new EventEmitter<{ pictures: number[], bill_amount: number }>();

  constructor() {
  }

  ngOnInit(): void {
    this.pbDFormGroup = new FormGroup({
      pictures: new FormControl([]),
      bill_amount: new FormControl(null, Validators.min(0))
    });
  }

  // onOKClick will call emit on the pbdValueEmitter and send the picture_array and bill_amount to the parent form
  onOKClick(): void {
    this.pbdValueEmitter.emit({
      pictures: this.pbDFormGroup.controls.pictures.value,
      bill_amount: this.pbDFormGroup.controls.bill_amount.value
    });
  }
}
