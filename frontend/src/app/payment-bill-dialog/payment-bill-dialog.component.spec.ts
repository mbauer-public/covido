import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentBillDialogComponent } from './payment-bill-dialog.component';

describe('PaymentBillDialogComponent', () => {
  let component: PaymentBillDialogComponent;
  let fixture: ComponentFixture<PaymentBillDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentBillDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentBillDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
