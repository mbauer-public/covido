import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import {DateComponent} from './date/date.component';
import {JwtModule} from '@auth0/angular-jwt';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { FileUploadModule } from 'ng2-file-upload';
import {MediainputComponent} from './mediainput/mediainput.component';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
import { TestComponent } from './test/test.component';

import { RegisterComponent } from './register/register.component';
import { ShoppinglistListComponent } from './shoppinglist-list/shoppinglist-list.component';
import {ShoppinglistFormComponent} from './shoppinglist-form/shoppinglist-form.component';
import {WalletComponent} from './wallet/wallet.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { PaymentBillDialogComponent } from './payment-bill-dialog/payment-bill-dialog.component';
import {HttperrorInterceptor} from './httperror.interceptor';
import { RatingDialogComponent } from './rating-dialog/rating-dialog.component';
import { AccountComponent } from './account/account.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';




@NgModule({
  declarations: [
    MediainputComponent,
    DateComponent,
    LoginComponent,
    LogoutComponent,
    AppComponent,
    TestComponent,
    RegisterComponent,
    ShoppinglistListComponent,
    ShoppinglistFormComponent,
    WalletComponent,
    PaymentBillDialogComponent,
    RatingDialogComponent,
    AccountComponent,
  ],
  entryComponents: [PaymentBillDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatNativeDateModule,
    FileUploadModule,
    MatIconModule,
    MatDialogModule,
    MatSidenavModule,
    MatListModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('access_token');
        },
        allowedDomains: ['localhost:4200']
      }
    }),
    MatSnackBarModule,
    MatSlideToggleModule,
    NgbModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttperrorInterceptor,
      multi: true,
      deps: [MatSnackBar]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
